import { FC } from 'react';

import { StyledMenu, MenuItem, MenuLink } from './styles';

type Props = {
  list: Array<{
    url: string;
    txt: string;
  }>;
  secondary?: boolean;
};

const Menu: FC<Props> = ({ list, secondary }) => (
  <StyledMenu>
    {
      list.map((item, idx) => (
        <MenuItem key={idx}>
          <MenuLink
            to={item.url !== '/' ? `/${item.url}/` : ''}
            secondary={secondary}
          >
            {item.txt}
          </MenuLink>
        </MenuItem>
      ))
    }
  </StyledMenu>
);

export default Menu;
