import styled from 'styled-components/macro';
import { NavLink } from 'react-router-dom';

type MenuLinkProps = {
  secondary?: boolean;
};

export const StyledMenu = styled.ul`
  display: grid;
  grid-auto-flow: column;
  justify-content: space-evenly;
  align-items: center;
  grid-gap: ${({ theme }) => theme.common.indent}px;
`;

export const MenuItem = styled.li``;

export const MenuLink = styled(NavLink)<MenuLinkProps>`
  display: block;
  padding: 10px 0;
  font-size: 14px;
  text-align: center;
  color: ${({ theme, secondary }) => (secondary ? theme.colors.light : theme.colors.main)};
  
  :hover,
  &.active {
    color: ${({ theme }) => theme.colors.secondary};
  }
`;
