import { FC, useEffect } from 'react';
import {
  Routes, Route, Navigate, useLocation, useNavigate
} from 'react-router-dom';

import AuthPage from 'pages/AuthPage';
import MainPage from 'pages/MainPage';
import MyAccountPage from 'pages/account/MyAccountPage';
import GenerateQuotePage from 'pages/GenerateQuotePage';
import NotFoundPage from 'pages/NotFoundPage';

import HeaderContainer from 'components/HeaderContainer';
import Footer from 'components/Footer';
import ErrorBoundary from 'components/ErrorBoundary';
import { useUser, useUserActions } from 'ducks/user';

import GlobalStyle from 'styles/global';
import { Container } from 'styles/common';
import {
  StyledApp, AppMain, AppTop, AppBottom
} from './styles';

const App: FC = () => {
  const { isAuth } = useUser();
  const { userAuth } = useUserActions();
  const { pathname } = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    if (pathname.slice(-1) !== '/') {
      navigate(`${pathname}/`, { replace: true });
    }
  }, [pathname, navigate]);

  useEffect(() => {
    if (localStorage.getItem('token')) {
      userAuth();
    }
  }, [userAuth]);

  return (
    <StyledApp background={!isAuth}>
      <GlobalStyle />
      <AppTop>
        <Container>
          <HeaderContainer />
        </Container>
      </AppTop>
      <AppMain background={isAuth}>
        <ErrorBoundary>
          {!isAuth ? (
            <Routes>
              <Route path="/login/" element={<AuthPage />} />
              <Route path="/signup/" element={<AuthPage />} />
              <Route path="*" element={<Navigate replace to="/login/" />} />
            </Routes>
          ) : (
            <Routes>
              <Route path="/" element={<MainPage />} />
              <Route path="/account/" element={<MyAccountPage />} />
              <Route path="/generate-quote/" element={<GenerateQuotePage />} />
              <Route path="/login/" element={<Navigate replace to="/" />} />
              <Route path="/signup/" element={<Navigate replace to="/" />} />
              <Route path="*" element={<NotFoundPage />} />
            </Routes>
          )}
        </ErrorBoundary>
      </AppMain>
      <AppBottom>
        <Footer />
      </AppBottom>
    </StyledApp>
  );
};

export default App;
