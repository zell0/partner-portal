import styled, { css } from 'styled-components/macro';

import bgImage from 'assets/img/bg/bg-main.svg';
import bgImageAccount from 'assets/img/bg/bg-inner.svg';

type Props = {
  background?: boolean;
};

export const StyledApp = styled.div<Props>`
  min-height: 100%;
  display: grid;
  grid-template-rows: auto 1fr auto;
  grid-template-columns: 100%;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;

  ${({ background }) => background && css`
    color: ${({ theme }) => `${theme.colors.light} !important`};
    background-image: url(${bgImage});
  `}
`;

export const AppTop = styled.div``;

export const AppMain = styled.div<Props>`
  padding: 50px 0;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  
  ${({ background }) => background && css`
    background-image: url(${bgImageAccount});
  `}
`;

export const AppBottom = styled.div`
  padding: 20px 0;
`;
