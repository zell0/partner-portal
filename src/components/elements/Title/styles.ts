import styled, { css } from 'styled-components/macro';

import { device } from 'constants/device';
import { Props } from './types';

export const StyledTitle = styled.h1<Props>`
  font-size: calc(24px + 4 * (100vw / ${({ theme }) => theme.common.containerWidth} ));
  font-weight: ${({ bold }) => (bold ? 'bold' : 'normal')};
  color: ${({ color, theme }) => color || theme.colors.main};
  margin: 0 0 ${({ theme }) => theme.common.indent}px;
  text-align: ${({ align }) => align || 'left'};

  ${({ as }) => as === 'h2' && css`
    font-size: calc(20px + 2 * (100vw / ${({ theme }) => theme.common.containerWidth} ));
  `}

  ${({ as }) => (as === 'h3' || as === 'h4') && css`
    font-size: 17px;
  `}

  ${({ as }) => (as === 'h5' || as === 'h6') && css`
    font-size: 16px;
  `}

  ${device.tablet} {
    text-align: center;
  }
`;
