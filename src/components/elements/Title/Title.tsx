import { FC } from 'react';

import { Props } from './types';

import { StyledTitle } from './styles';

const Title: FC<Props> = ({ children, ...props }) => (
  <StyledTitle {...props}>
    {children}
  </StyledTitle>
);

export default Title;
