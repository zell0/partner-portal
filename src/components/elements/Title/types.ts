import { ReactNode } from 'react';

export type Props = {
  readonly as?: React.ElementType | keyof JSX.IntrinsicElements;
  readonly align?: 'left' | 'center' | 'right';
  readonly color?: string;
  readonly bold?: boolean;
  readonly children: ReactNode;
};
