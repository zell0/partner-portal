import { FC } from 'react';
import { ButtonProps } from '@material-ui/core/Button';

import { StyledButton } from './styles';

import { Props } from './types';

const Button: FC<Props & ButtonProps> = ({ children, ...props }) => (
  <StyledButton {...props}>
    {children}
  </StyledButton>
);

export default Button;
