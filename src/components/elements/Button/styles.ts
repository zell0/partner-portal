import styled, { css } from 'styled-components/macro';
import Button, { ButtonProps } from '@material-ui/core/Button';
import { lighten, darken } from 'polished';

import { Props } from './types';

export const StyledButton = styled(Button)<Props & ButtonProps>`
  min-width: 160px;
  height: ${({ size }) => (size === 'medium' ? '40px' : '50px')};
  padding: 0 20px;
  font-size: 11px;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.light};
  text-align: center;
  border-radius: 8px;
  background-color: ${({ theme }) => theme.colors.accentSec};
  
  :hover {
    background-color: ${({ theme }) => darken(0.08, theme.colors.accentSec)};
  }
  
  :disabled {
    color: ${({ theme }) => theme.colors.light};
    opacity: 0.75;
  }
  
  ${({ $gradient }) => $gradient && css`
    background: linear-gradient(
      90deg,
      ${({ theme }) => theme.colors.accentSec} 0,
      ${({ theme }) => lighten(0.2, theme.colors.accentSec)} 100%
    );
  `}
`;
