import { FC } from 'react';
import { TypographyProps } from '@material-ui/core/Typography';

import { Props } from './types';

import { StyledSubtitle } from './styles';

const Subtitle: FC<Props & TypographyProps> = ({ children, ...props }) => (
  <StyledSubtitle {...props}>
    {children}
  </StyledSubtitle>
);

export default Subtitle;
