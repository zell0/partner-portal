import styled, { css } from 'styled-components/macro';
import Typography, { TypographyProps } from '@material-ui/core/Typography';

import { Props } from './types';

export const StyledSubtitle = styled(Typography)<Props & TypographyProps>`
  margin-bottom: 20px;

  ${({ marginbottom }) => marginbottom === 'large' && css`
    margin-bottom: 50px;
  `}

  ${({ marginbottom }) => marginbottom === 'medium' && css`
    margin-bottom: 30px;
  `}

  ${({ marginbottom }) => marginbottom === 'small' && css`
    margin-bottom: 10px;
  `}
`;
