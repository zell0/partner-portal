import styled, { css, keyframes } from 'styled-components/macro';

import { Props } from './types';

export const StyledLoader = styled.div<Props>`
  display: flex;
  justify-content: center;
  align-items: center;

  ${({ spaceRight }) => spaceRight && css`
    margin-right: 10px;
  `}
`;

export const Wrapper = styled.div<Props>`
  position: relative;
  width: ${({ size }) => (size ? `${size}px` : '70px')};
  height: ${({ size }) => (size ? `${size}px` : '70px')};
`;

const rotate = keyframes`
  0 % {
    transform: rotate(0deg);
  }
  50% {
    transform: rotate(180deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

export const Inner = styled.div<Props>`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  border-radius: 50%;
  animation: ${rotate} .8s linear infinite;
  box-shadow: 0 ${({ thin }) => (thin ? '1px' : '3px')} 0 0 ${({ theme, color }) => color ?? theme.colors.accent};
`;
