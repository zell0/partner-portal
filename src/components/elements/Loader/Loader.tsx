import { FC } from 'react';

import { StyledLoader, Wrapper, Inner } from './styles';
import { Props } from './types';

const Loader: FC<Props> = ({
  size, color, thin, ...props
}) => (
  <StyledLoader {...props}>
    <Wrapper size={size}>
      <Inner color={color} thin />
    </Wrapper>
  </StyledLoader>
);

export default Loader;
