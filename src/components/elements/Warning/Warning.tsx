import { FC } from 'react';

import { ReactComponent as IconWarning } from 'assets/img/icons/warning.svg';

import { StyledWarning, Icon, Text } from './styles';

type Props = {
  txt: string;
};

const Warning: FC<Props> = ({ txt }) => (
  <StyledWarning>
    <Icon>
      <IconWarning title="Warning" />
    </Icon>
    <Text>{txt}</Text>
  </StyledWarning>
);

export default Warning;
