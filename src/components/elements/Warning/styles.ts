import styled from 'styled-components/macro';
import { lighten } from 'polished';

export const StyledWarning = styled.div`
  display: flex;
  align-items: flex-start;
  padding: 10px;
  border-radius: 8px;
  border: 1px solid ${({ theme }) => theme.colors.error};
  background-color: ${({ theme }) => lighten(0.48, theme.colors.error)};
`;

export const Icon = styled.div`
  flex-shrink: 0;
  max-width: 24px;
  margin-right: 10px;
  
  img, svg {
    display: block;
  }
`;

export const Text = styled.div``;
