import { FC } from 'react';

import logo from 'assets/img/logo.svg';
import logoSecondary from 'assets/img/logo-light.svg';

import { StyledLogo } from './styles';

type Props = {
  secondary?: boolean;
};

const Logo: FC<Props> = ({ secondary }) => (
  <StyledLogo to="/">
    <img
      src={secondary ? logoSecondary : logo}
      width="144"
      height="22"
      alt="Nakivo"
    />
  </StyledLogo>
);

export default Logo;
