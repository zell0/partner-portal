import styled from 'styled-components/macro';
import { NavLink } from 'react-router-dom';

export const StyledLogo = styled(NavLink)`
  
  img {
    display: block;
  }
`;
