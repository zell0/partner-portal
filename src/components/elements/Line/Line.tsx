import { FC } from 'react';

import { StyledLine } from './styles';

const Line: FC = () => (
  <StyledLine />
);

export default Line;
