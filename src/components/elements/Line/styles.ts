import styled from 'styled-components/macro';

export const StyledLine = styled.hr`
  width: 100%;
  height: 1px;
  border: none;
  margin: 8px 0;
  background-color: ${({ theme }) => theme.colors.border};
`;
