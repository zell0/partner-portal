import { FC } from 'react';

import Logo from 'components/elements/Logo';
import Menu from 'components/Menu';
import Loader from 'components/elements/Loader';
import UserDropdown from 'components/UserDropdown';
import ErrorIndicator from 'components/ErrorIndicator';
import { useUser } from 'ducks/user';
import { menu, authMenu, userMenu } from 'utils/data';

import {
  StyledHeader, Left, Center, Right
} from './styles';

const HeaderContainer: FC = () => {
  const {
    user, isAuth, isLoading, error
  } = useUser();

  if (!isAuth) {
    return (
      <StyledHeader>
        <Left>
          <Logo secondary />
        </Left>
        <Center />
        <Right>
          <Menu list={authMenu} secondary />
        </Right>
      </StyledHeader>
    );
  }

  return (
    <StyledHeader>
      <Left>
        <Logo />
      </Left>
      {error && <ErrorIndicator message={error} tagName="span" />}
      <Center>
        <Menu list={menu} />
      </Center>
      <Right>
        {isLoading && <Loader size={30} />}
        {user && <UserDropdown userData={user} list={userMenu} />}
      </Right>
    </StyledHeader>
  );
};

export default HeaderContainer;
