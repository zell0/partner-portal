import styled from 'styled-components/macro';

export const StyledHeader = styled.header`
  display: grid;
  grid-template-columns: minmax(100px, auto) 1fr 180px;
  align-items: center;
  grid-gap: ${({ theme }) => theme.common.gutter}px;
  padding: 20px 0;
`;

export const Left = styled.div`
  max-width: 180px;
`;

export const Center = styled.div``;

export const Right = styled.div``;
