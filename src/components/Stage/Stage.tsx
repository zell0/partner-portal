import { FC } from 'react';

import {
  StyledStage, Item, Top, Count, StageTitle
} from './styles';

const Stage: FC = () => (
  <StyledStage>
    <Item completed>
      <Top>
        <Count>1</Count>
      </Top>
      <StageTitle>Order details</StageTitle>
    </Item>
    <Item active>
      <Top>
        <Count>2</Count>
      </Top>
      <StageTitle>Customer details</StageTitle>
    </Item>
    <Item>
      <Top>
        <Count>3</Count>
      </Top>
      <StageTitle>Checkout</StageTitle>
    </Item>
  </StyledStage>
);

export default Stage;
