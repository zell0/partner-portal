import styled, { css } from 'styled-components/macro';

import iconCheck from 'assets/img/icons/check.svg';

type Props = {
  active?: boolean;
  completed?: boolean;
};

export const StyledStage = styled.div`
  display: flex;
  justify-content: center;
`;

export const Top = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  margin-bottom: 8px;

  ::before {
    content: '';
    position: absolute;
    top: calc(50% - 1px);
    left: 0;
    width: 100%;
    height: 2px;
    background-color: ${({ theme }) => theme.colors.border};
  }
`;

export const Count = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 32px;
  height: 32px;
  border-radius: 50%;
  font-size: 14px;
  color: ${({ theme }) => theme.colors.light};
  background-color: ${({ theme }) => theme.colors.border};
  z-index: 2;
`;

export const StageTitle = styled.span`
  font-size: 11px;
  color: ${({ theme }) => theme.colors.border};
  text-transform: uppercase;
`;

export const Item = styled.div<Props>`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  max-width: 232px;

  :first-child {
    
    ${Top}::before {
      width: 50%;
      left: auto;
      right: 0;
    }
  }
  
  :last-child {

    ${Top}::before {
      width: 50%;
    }
  }

  ${({ active }) => active && css`
    
    ${Top}::before {
      background-color: ${({ theme }) => theme.colors.light};
    }
    
    ${Count} {
      color: ${({ theme }) => theme.colors.main};
      background-color: ${({ theme }) => theme.colors.light};
      box-shadow: inset 0 0 0 2px ${({ theme }) => theme.colors.accent};
    }
    
    ${StageTitle} {
      color: ${({ theme }) => theme.colors.main};
    }
  `}

  ${({ completed }) => completed && css`
    
    ${Top}::before {
      background-color: ${({ theme }) => theme.colors.accent};
    }
    
    ${Count} {
      color: transparent;
      background: ${({ theme }) => theme.colors.accent} url(${iconCheck}) no-repeat center;
    }
    
    ${StageTitle} {
      color: ${({ theme }) => theme.colors.accent};
    }
  `}
`;
