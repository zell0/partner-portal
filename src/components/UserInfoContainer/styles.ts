import styled from 'styled-components/macro';
import { lighten } from 'polished';
import { NavLink } from 'react-router-dom';

export const StyledUserInfoContainer = styled.div`
  padding: ${({ theme }) => theme.common.gutter}px;
  background-color: ${({ theme }) => theme.colors.light};
  border-radius: 12px;
  box-shadow: 0 8px 32px ${({ theme }) => lighten(0.35, theme.colors.accent)};
`;

export const Avatar = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 80px;
  height: 80px;
  border-radius: 50%;
  margin: -72px 0 30px;
  background: linear-gradient(
    270deg,
    ${({ theme }) => lighten(0.2, theme.colors.accent)} 0%,
    ${({ theme }) => theme.colors.accent} 100%
  );
`;

export const Item = styled.div`
  margin-bottom: 40px;
`;

export const Title = styled.h4`
  position: relative;
  display: flex;
  align-items: center;
  font-size: 12px;
  color: ${({ theme }) => theme.colors.title};
  text-transform: uppercase;
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};
  padding-bottom: 10px;
  margin-bottom: 20px;
`;

export const Name = styled.h6`
  font-size: 21px;
  margin: 0 0 20px;
`;

export const Contact = styled.div`
  display: flex;
  align-items: flex-start;
  margin-bottom: 20px;
  
  a {
    
    :hover {
      color: ${({ theme }) => theme.colors.title};
    }
  }
`;

export const Icon = styled.img`
  flex-shrink: 0;
  padding-top: 2px;
  margin-right: 15px;
`;

export const LinkEdit = styled(NavLink)`
  flex-shrink: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 32px;
  height: 32px;
  border-radius: 8px;
  border: 1px solid ${({ theme }) => theme.colors.border};
  margin-left: auto;
`;

export const Text = styled.div`
  font-size: 14px;
`;
