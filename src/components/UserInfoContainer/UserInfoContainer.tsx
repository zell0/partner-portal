import { FC } from 'react';

import { useUser } from 'ducks/user';
import icoUser from 'assets/img/icons/user.svg';
import icoMail from 'assets/img/icons/email.svg';
import icoPhone from 'assets/img/icons/phone.svg';
import icoLocation from 'assets/img/icons/location.svg';
import icoEdit from 'assets/img/icons/edit.svg';

import {
  StyledUserInfoContainer, Avatar, Item, Title, Name, Contact, Icon, Text, LinkEdit
} from './styles';

const UserInfoContainer: FC = () => {
  const { user } = useUser();

  return (
    <StyledUserInfoContainer>
      <Avatar>
        <img src={user?.user.avatar || icoUser} alt={user?.user.name} />
      </Avatar>
      <Item>
        <Title>Salepersons</Title>
        <Name>Sergey Sergey</Name>
        <Contact>
          <Icon src={icoMail} />
          <a href="mailto:sergey.sergey@nakivo.com">sergey.sergey@nakivo.com</a>
        </Contact>
        <Contact>
          <Icon src={icoPhone} />
          <Text>063723745438743235475</Text>
        </Contact>
        <Contact>
          <Icon src={icoLocation} />
          <Text>2464 Royal Ln. Mesa, New Jersey 45463 2464 Royal Ln. Mesa, New Jersey 45463</Text>
        </Contact>
      </Item>
      <Item>
        <Title>
          Your details
          <LinkEdit to="/settings/">
            <img src={icoEdit} alt="edit" />
          </LinkEdit>
        </Title>
        <Name>
          {user?.user.name}
          {' '}
          {user?.user.lastname}
        </Name>
        <Contact>
          <Icon src={icoMail} />
          <a href={`mailto:${user?.user.email}`}>{user?.user.email}</a>
        </Contact>
        <Contact>
          <Icon src={icoPhone} />
          <Text>{user?.user.phone}</Text>
        </Contact>
        <Contact>
          <Icon src={icoLocation} />
          <Text>{user?.user.address}</Text>
        </Contact>
      </Item>
    </StyledUserInfoContainer>
  );
};

export default UserInfoContainer;
