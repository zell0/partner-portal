import { FC } from 'react';

import { StyledFooter } from './styles';

const Footer: FC = () => (
  <StyledFooter>
    Footer
  </StyledFooter>
);

export default Footer;
