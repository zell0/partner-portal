import styled from 'styled-components/macro';

export const StyledAside = styled.div`
  padding: ${({ theme }) => theme.common.gutter}px;
  background-color: ${({ theme }) => theme.colors.light};
  border-radius: 12px;
  box-shadow: 0 8px 32px rgb(0 114 250 / 10%);
`;
