import { FC } from 'react';

import { StyledAside } from './styles';

const Aside: FC = () => (
  <StyledAside>
    Aside
  </StyledAside>
);

export default Aside;
