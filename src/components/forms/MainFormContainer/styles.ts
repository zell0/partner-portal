import styled from 'styled-components/macro';
import { Form } from 'formik';

export const StyledForm = styled(Form)``;

export const Row = styled.div`
  display: grid;
  grid-auto-flow: column;
  justify-content: start;
  grid-gap: ${({ theme }) => theme.common.gutter}px;
  margin-bottom: ${({ theme }) => theme.common.gutter + 10}px;
`;

export const Col = styled.div`
  max-width: 187px;
`;
