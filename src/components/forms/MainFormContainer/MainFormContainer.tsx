import { FC, useEffect } from 'react';
import { Formik } from 'formik';

import Loader from 'components/elements/Loader';
import Title from 'components/elements/Title';
import Subtitle from 'components/elements/Subtitle';
import ErrorIndicator from 'components/ErrorIndicator';
import FormControl from 'components/forms/FormControl';
import Button from 'components/elements/Button';

import { useForm, useFormActions } from 'ducks/form';

import { StyledForm, Row, Col } from './styles';

type Values = {
  [key: string]: number;
};

const MainFormContainer: FC = () => {
  const { fields, isLoading, error } = useForm();
  const { formFetchRequest, formSet } = useFormActions();

  const initialValues: Values = {};
  fields.forEach((field) => {
    initialValues[field.name] = field.value;
  });

  const submitFields = (values: Values) => {
    formSet(fields.map((field) => ({
      ...field,
      value: values[field.name]
    })));
  };

  useEffect(() => formFetchRequest(), [formFetchRequest]);

  if (isLoading) {
    return <Loader />;
  }

  if (error) {
    return <ErrorIndicator message={error} />;
  }

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values, actions) => {
        submitFields(values);
        const timeOut = setTimeout(() => {
          actions.setSubmitting(false);
          clearTimeout(timeOut);
        }, 1000);
      }}
    >
      {({
        values,
        errors,
        touched,
        handleSubmit,
        isSubmitting,
        isValidating,
        isValid
      }) => (
        <StyledForm>
          <Title as="h2">Customer Environment</Title>

          <Subtitle>Virtual</Subtitle>
          <Row>
            {
              fields.filter((item) => item.category === 'virtual').map((field, idx) => (
                <Col key={idx}>
                  <FormControl
                    id={field.name}
                    type={field.type}
                    name={field.name}
                    disableUnderline
                    label={field.name}
                  />
                </Col>
              ))
            }
          </Row>

          <Subtitle>Physical</Subtitle>
          <Row>
            {
              fields.filter((item) => item.category === 'physical').map((field, idx) => (
                <Col key={idx}>
                  <FormControl
                    id={field.name}
                    type={field.type}
                    name={field.name}
                    disableUnderline
                    label={field.name}
                  />
                </Col>
              ))
            }
          </Row>

          <Row>
            <Col>
              <Subtitle>Cloud</Subtitle>
              {
                fields.filter((item) => item.category === 'cloud').map((field, idx) => (
                  <Col key={idx}>
                    <FormControl
                      id={field.name}
                      type={field.type}
                      name={field.name}
                      disableUnderline
                      label={field.name}
                    />
                  </Col>
                ))
              }
            </Col>

            <Col>
              <Subtitle>Applications</Subtitle>
              {
                fields.filter((item) => item.category === 'applications').map((field, idx) => (
                  <Col key={idx}>
                    <FormControl
                      id={field.name}
                      type={field.type}
                      name={field.name}
                      disableUnderline
                      disabled
                      label={field.name}
                    />
                  </Col>
                ))
              }
            </Col>
          </Row>

          <Row>
            <Col>
              <Subtitle>SaaS</Subtitle>
              {
                fields.filter((item) => item.category === 'saas').map((field, idx) => (
                  <Col key={idx}>
                    <FormControl
                      id={field.name}
                      type={field.type}
                      name={field.name}
                      disableUnderline
                      label={field.name}
                    />
                  </Col>
                ))
              }
            </Col>
            <Col>
              <Subtitle>&nbsp;</Subtitle>
              Range
            </Col>
          </Row>
          <Row>
            <Button type="submit" $gradient disabled={!isValid || isSubmitting}>Next</Button>
          </Row>
        </StyledForm>
      )}
    </Formik>
  );
};

export default MainFormContainer;
