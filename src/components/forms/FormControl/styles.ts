import styled, { css } from 'styled-components/macro';
import { FormControl, Input, InputProps } from '@material-ui/core';
import { lighten } from 'polished';

type Props = {
  size?: 'small' | 'medium';
};

export const StyledFormControl = styled(FormControl)`
  width: 100%;
`;

export const Field = styled(Input).attrs({
  classes: { root: 'root', input: 'input', focused: 'focused' }
})<Props & InputProps>`
  && {
    height: 50px;
    margin: 0;
    border: 1px solid ${({ theme, error }) => (error ? theme.colors.error : theme.colors.border)};
    border-radius: 8px;
    background: linear-gradient(
      180deg,
      ${({ theme }) => theme.colors.borderLight} 10.42%,
      ${({ theme }) => theme.colors.light} 17.71%
    );
    transition: .2s ease;

    ${({ size }) => size === 'small' && css`
      height: 30px;
    `}

    ${({ size }) => size === 'medium' && css`
      height: 40px;
    `}

    ${({ disabled }) => disabled && css`
      background: ${({ theme }) => theme.colors.borderLight};
    `}

    &.focused {
      border-color: ${({ theme }) => lighten(0.2, theme.colors.accent)};
    }
  }
  
  .input {
    height: 100%;
    padding: 0 12px;
  }
`;

export const Label = styled.label`
  font-size: 13px;
  margin-bottom: 8px;
`;

export const Error = styled.div`
  font-size: 12px;
  color: ${({ theme }) => theme.colors.error};
  margin-top: 3px;
`;
