import { FC } from 'react';
import { InputProps } from '@material-ui/core';
import { useField, FieldHookConfig } from 'formik';

import {
  StyledFormControl, Field, Label, Error
} from './styles';

type Props = FieldHookConfig<string> & {
  label: string;
  size?: 'small' | 'medium';
};

const FormControl: FC<Props & InputProps> = ({ label, ...props }) => {
  const [field, meta] = useField(props);

  return (
    <StyledFormControl>
      <Label htmlFor={props.id}>{label}</Label>
      <Field {...field} {...meta} {...props} disableUnderline />
      {meta.error && meta.touched && (
        <Error>{meta.error}</Error>
      )}
    </StyledFormControl>
  );
};

export default FormControl;
