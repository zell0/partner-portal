import styled from 'styled-components/macro';
import { Form } from 'formik';

export const StyledAuthForm = styled(Form)`
  width: 100%;
  max-width: 350px;
  color: ${({ theme }) => theme.colors.main};
  background-color: ${({ theme }) => theme.colors.light};
  border-radius: 20px;
  padding: 30px 20px;
`;

export const Title = styled.h3`
  font-size: 21px;
  font-weight: bold;
  text-align: center;
  margin: 0 0 25px;
`;

export const Row = styled.div`
  margin-bottom: 20px;
`;

export const Bottom = styled.div`
  margin-top: 25px;
`;
