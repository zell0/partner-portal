import { FC } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import FormControl from 'components/forms/FormControl';
import Button from 'components/elements/Button';
import Warning from 'components/elements/Warning';
import Loader from 'components/elements/Loader';
import { UserRegType, useUser, useUserActions } from 'ducks/user';

import {
  StyledAuthForm, Title, Row, Bottom
} from 'components/forms/auth/styles';

type Values = UserRegType & {
  'password_confirm': string;
};

const RegisterFormContainer: FC = () => {
  const initialValues: Values = {
    name: '',
    lastname: '',
    email: '',
    password: '',
    password_confirm: ''
  };

  const SignupSchema = Yup.object().shape({
    name: Yup.string().min(2, 'Minimum 2 characters').max(20, 'Maximum 20 characters').required('Field is required'),
    lastname: Yup.string().min(2, 'Minimum 2 characters').max(20, 'Maximum 20 characters').required('Field is required'),
    email: Yup.string().email('Invalid email').required('Field is required'),
    password: Yup.string().min(6, 'Minimum 6 characters').max(20, 'Maximum 20 characters').required('Field is required'),
    password_confirm: Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match').required('Field is required')
  });

  const { isLoading, error } = useUser();
  const { userRegRequest } = useUserActions();

  const handleSubmit = (fields: Values) => userRegRequest(fields);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={SignupSchema}
      validateOnChange={false}
      validateOnBlur={false}
      initialErrors={initialValues}
      onSubmit={(values) => handleSubmit(values)}
      key="signup"
    >
      {({ isValid }) => (
        <StyledAuthForm>
          <Title>Sign up for the Partner Portal</Title>
          <Row>
            <FormControl
              id="name"
              type="text"
              name="name"
              label="Name"
              size="medium"
              autoFocus
            />
          </Row>
          <Row>
            <FormControl
              id="lastname"
              type="text"
              name="lastname"
              label="Lastname"
              size="medium"
            />
          </Row>
          <Row>
            <FormControl
              id="email"
              type="text"
              name="email"
              label="E-mail"
              size="medium"
            />
          </Row>
          <Row>
            <FormControl
              id="password"
              type="password"
              name="password"
              label="Password"
              size="medium"
            />
          </Row>
          <Row>
            <FormControl
              id="password_confirm"
              type="password"
              name="password_confirm"
              label="Confirm password"
              size="medium"
            />
          </Row>
          <Button type="submit" size="medium" fullWidth disabled={isLoading}>
            {isLoading && <Loader size={15} thin spaceRight />}
            Register
          </Button>
          {isValid && error && (
            <Bottom>
              <Warning txt={error} />
            </Bottom>
          )}
        </StyledAuthForm>
      )}
    </Formik>
  );
};

export default RegisterFormContainer;
