import { FC } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import FormControl from 'components/forms/FormControl';
import Button from 'components/elements/Button';
import Warning from 'components/elements/Warning';
import Loader from 'components/elements/Loader';
import { UserLoginType, useUser, useUserActions } from 'ducks/user';

import {
  StyledAuthForm, Title, Row, Bottom
} from 'components/forms/auth/styles';

const LoginFormContainer: FC = () => {
  const initialValues: UserLoginType = {
    email: '',
    password: ''
  };

  const LoginSchema = Yup.object().shape({
    email: Yup.string().email('Invalid email').required('Field is required'),
    password: Yup.string().min(6, 'Minimum 6 characters').max(20, 'Maximum 20 characters').required('Field is required'),
  });

  const { isLoading, error } = useUser();
  const { userLoginRequest } = useUserActions();

  const handleSubmit = (fields: UserLoginType) => userLoginRequest(fields);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={LoginSchema}
      validateOnChange={false}
      validateOnBlur={false}
      initialErrors={initialValues}
      onSubmit={(values) => handleSubmit(values)}
      key="login"
    >
      {({ isValid }) => (
        <StyledAuthForm>
          <Title>Log in to the Partner Portal</Title>
          <Row>
            <FormControl
              id="email"
              type="text"
              name="email"
              label="E-mail"
              size="medium"
              autoFocus
            />
          </Row>
          <Row>
            <FormControl
              id="password"
              type="password"
              name="password"
              label="Password"
              size="medium"
            />
          </Row>
          <Button type="submit" size="medium" fullWidth disabled={isLoading}>
            {isLoading && <Loader size={15} thin spaceRight />}
            Log in
          </Button>
          {isValid && error && (
            <Bottom>
              <Warning txt={error} />
            </Bottom>
          )}
        </StyledAuthForm>
      )}
    </Formik>
  );
};

export default LoginFormContainer;
