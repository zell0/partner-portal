import { FC } from 'react';

type Props = {
  message?: string | Error;
  tagName?: string;
};

const ErrorIndicator: FC<Props> = ({ message = 'Error! Please, try again later', tagName = 'h1' }) => {
  const Tag = tagName as keyof JSX.IntrinsicElements;

  return (
    <Tag>
      Error:
      {' '}
      {message}
    </Tag>
  );
};

export default ErrorIndicator;
