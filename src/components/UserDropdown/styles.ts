import styled from 'styled-components/macro';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { NavLink } from 'react-router-dom';

import iconArrow from 'assets/img/icons/arrow-down.svg';

type DropdownButtonProps = {
  open: boolean;
};

export const StyledDropdown = styled.div`
  width: 100%;
`;

export const DropdownTitle = styled.div`
  display: flex;
`;

export const Icon = styled.div`
  flex-shrink: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 32px;
  height: 32px;
  border-radius: 50%;
  background-color: ${({ theme }) => theme.colors.bgLight};
  margin-right: 10px;
  overflow: hidden;
  
  img {
    display: block;
  }
`;

export const DropdownButton = styled(Button).attrs({
  classes: { label: 'label' }
})<DropdownButtonProps>`
  padding: 0;
  
  ::after {
    flex-shrink: 0;
    content: '';
    width: 6px;
    height: 4px;
    background: url(${iconArrow}) no-repeat center;
    margin-left: 8px;
    transition: transform .2s ease;
    transform-origin: center;
    transform: rotate(${({ open }) => (open ? '-180deg' : '0deg')});
  }
  
  :hover {
    background-color: transparent;
  }
  
  .label {
    display: block;
    font-size: 13px;
    text-transform: none;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

export const StyledMenu = styled(Menu).attrs({
  classes: { paper: 'paper', list: 'list' }
})`
  
  .paper {
    border-radius: 10px;
  }
  
  .list {
    width: 100%;
    max-width: 290px;
    padding: 8px;
  }
`;

export const StyledMenuItem = styled(MenuItem)`
  padding: 0;
  border-radius: 6px;
  white-space: normal;
`;

export const Link = styled(NavLink)`
  width: 100%;
  padding: 6px 16px;
  
  &.active {
    background-color: rgba(0, 0, 0, 0.07);
  }
`;
