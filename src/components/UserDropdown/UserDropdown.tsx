import { FC, useState, MouseEvent } from 'react';

import Line from 'components/elements/Line';
import { UserType, useUserActions } from 'ducks/user';
import userIcon from 'assets/img/icons/user-defalut.svg';

import {
  StyledDropdown, DropdownTitle, Icon, DropdownButton, StyledMenu, StyledMenuItem, Link
} from './styles';

type Props = {
  userData: UserType;
  list: Array<{
    url: string;
    txt: string;
  }>;
};

const UserDropdown: FC<Props> = ({ userData, list }) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const { userLogout } = useUserActions();

  const handleClickButton = (e: MouseEvent<HTMLButtonElement>) => setAnchorEl(e.currentTarget);

  const handleClose = () => setAnchorEl(null);

  return (
    <StyledDropdown>
      <DropdownTitle>
        <Icon>
          <img src={userData.user.avatar ?? userIcon} alt={`${userData.user.name} ${userData.user.lastname}`} />
        </Icon>
        <DropdownButton
          aria-controls="user-menu"
          aria-haspopup="true"
          open={Boolean(anchorEl)}
          onClick={handleClickButton}
        >
          {`${userData.user.name.slice(0, 1)}. ${userData.user.lastname}`}
        </DropdownButton>
      </DropdownTitle>
      <StyledMenu
        id="user-menu"
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        {
          list.map((item, idx) => (
            <StyledMenuItem key={idx} onClick={handleClose}>
              <Link to={`/${item.url}/`}>
                {item.txt}
              </Link>
            </StyledMenuItem>
          ))
        }
        <Line />
        <StyledMenuItem onClick={userLogout}>
          <Link to="/logout/">Logout</Link>
        </StyledMenuItem>
      </StyledMenu>
    </StyledDropdown>
  );
};

export default UserDropdown;
