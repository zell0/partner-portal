import { Component, ReactNode } from 'react';

import Title from 'components/elements/Title';

import { Container } from 'styles/common';

type Props = {
  children: ReactNode;
};

type State = {
  error: Error | null;
};

class ErrorBoundary extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);

    this.state = {
      error: null
    };
  }

  public componentDidCatch(error: Error): void {
    this.setState({ error });
  }

  public render(): ReactNode {
    if (this.state.error) {
      return (
        <Container>
          <Title bold>Error! Please, try again later</Title>
        </Container>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
