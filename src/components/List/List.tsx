import { FC } from 'react';

import { StyledList, Link, Icon } from './styles';

type Props = {
  list: Array<{
    url: string;
    txt: string;
    icon: string;
  }>;
};

const List: FC<Props> = ({ list }) => (
  <StyledList>
    {
      list.map((link) => (
        <Link key={link.url} to={`${link.url}/`}>
          <Icon src={link.icon} />
          {link.txt}
        </Link>
      ))
    }
  </StyledList>
);

export default List;
