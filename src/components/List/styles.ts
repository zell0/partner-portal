import styled from 'styled-components/macro';
import { NavLink } from 'react-router-dom';

export const StyledList = styled.div`
  border-radius: 12px;
  background-color: ${({ theme }) => theme.colors.light};
  border: 1px solid ${({ theme }) => theme.colors.border};
`;

export const Link = styled(NavLink)`
  display: flex;
  align-items: initial;
  color: ${({ theme }) => theme.colors.title};
  font-size: 17px;
  font-weight: bold;
  padding: 30px;
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};
  
  :hover {
    color: ${({ theme }) => theme.colors.main};
  }
  
  :last-child {
    border-bottom: none;
  }
`;

export const Icon = styled.img`
  flex-shrink: 0;
  max-width: 24px;
  margin-right: 24px;
`;
