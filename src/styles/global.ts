import { createGlobalStyle } from 'styled-components';
import styledNormalize from 'styled-normalize';

const GlobalStyle = createGlobalStyle`
  ${styledNormalize}

  * {
    box-sizing: border-box;

    ::before,
    ::after {
      box-sizing: border-box;
    }
  }
  html, body {
    height: 100%;
  }
  body {
    font-family: ${({ theme }) => theme.fonts.main};
    font-size: 16px;
    min-width: 320px;
    min-height: 1px;
    position: relative;
    line-height: 1.5;
    color: ${({ theme }) => theme.colors.main};
    overflow-wrap: break-word;
    word-wrap: break-word;
    overflow-x: hidden;
  }
  a {
    color: ${({ theme }) => theme.colors.main};
    text-decoration: none;
    transition: .2s ease;

    :active,
    :focus {
      text-decoration: none;
    }
  }
  img {
    max-width: 100%;
    height: auto;
  }
  ul {
    list-style: none;
    margin: 0;
    padding: 0;
  }
  #root {
    height: 100%;
  }
`;

export default GlobalStyle;
