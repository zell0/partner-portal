const mediaQuery = (size: number): string => `@media (max-width: ${size}px)`;

export default mediaQuery;
