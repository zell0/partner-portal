import iconSuitcase from 'assets/img/icons/suitcase.svg';
import iconQuotation from 'assets/img/icons/quotation.svg';
import iconSales from 'assets/img/icons/sales-order.svg';
import iconInvoices from 'assets/img/icons/invoice.svg';

type UserAccountListType = {
  url: string;
  txt: string;
  icon: string;
}

const userAccountList: Array<UserAccountListType> = [
  {
    url: 'opportunities',
    txt: 'My Opportunities',
    icon: iconSuitcase
  },
  {
    url: 'quotations',
    txt: 'Quotations',
    icon: iconQuotation
  },
  {
    url: 'sales-orders',
    txt: 'Sales Orders',
    icon: iconSales
  },
  {
    url: 'invoices',
    txt: 'Invoices',
    icon: iconInvoices
  }
];

export default userAccountList;
