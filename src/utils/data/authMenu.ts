type AuthMenuType = {
  url: string,
  txt: string
}

const authMenu: Array<AuthMenuType> = [
  {
    url: 'login',
    txt: 'Login'
  },
  {
    url: 'signup',
    txt: 'Sign up'
  }
];

export default authMenu;
