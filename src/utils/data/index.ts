import menu from './menu';
import authMenu from './authMenu';
import userMenu from './userMenu';

export {
  menu,
  authMenu,
  userMenu
};
