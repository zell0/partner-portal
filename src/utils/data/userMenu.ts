type UserMenuType = {
  url: string,
  txt: string
}

const userMenu: Array<UserMenuType> = [
  {
    url: 'account',
    txt: 'My account'
  },
  {
    url: 'settings',
    txt: 'Settings'
  }
];

export default userMenu;
