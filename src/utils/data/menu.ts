type MenuType = {
  url: string;
  txt: string;
}

const menu: Array<MenuType> = [
  {
    url: '/',
    txt: 'Buy New License'
  },
  {
    url: 'generate-quote',
    txt: 'Generate Quote'
  },
  {
    url: 'resources',
    txt: 'Resources'
  },
  {
    url: 'partners',
    txt: 'Partner Program'
  },
  {
    url: 'training',
    txt: 'Training'
  },
  {
    url: 'contacts',
    txt: 'Contact Us'
  }
];

export default menu;
