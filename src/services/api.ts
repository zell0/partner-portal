import axios, { AxiosResponse } from 'axios';

import { UserType } from 'ducks/user';

export const URL_BASE = 'http://localhost:5000';

const api = axios.create({
  withCredentials: true,
  baseURL: URL_BASE
});

export const getAuthRefresh = (): Promise<AxiosResponse<UserType>> => axios.get(
  `${URL_BASE}/refresh`,
  { withCredentials: true }
);

api.interceptors.request.use((config) => {
  const conf = config;

  if (conf.headers) {
    conf.headers.Authorization = `Bearer ${localStorage.getItem('token')}`;
  }

  return conf;
});

api.interceptors.response.use((config) => config, async (error) => {
  const origRequest = error.config;

  if (error.response.status === 401 && error.config && !error.config._isRetry) {
    origRequest._isRetry = true;

    try {
      const response = await getAuthRefresh();
      localStorage.setItem('token', response.data.accessToken);
      return api.request(origRequest);
    } catch (e) {
      return e;
    }
  }

  throw error;
});

export default api;
