import { AxiosResponse } from 'axios';

import api, { getAuthRefresh } from 'services/api';
import { UserType } from 'ducks/user';

type AuthApiType = {
  login: (email: string, password: string) => Promise<AxiosResponse<UserType>>;
  registration: (email: string, password: string, name: string, lastname: string) => Promise<AxiosResponse<UserType>>;
  auth: () => Promise<AxiosResponse<UserType>>;
  logout: () => Promise<void>;
};

const authApi: AuthApiType = {
  login: (email, password) => api.post<UserType>(
    '/login', { email, password }
  ),

  registration: (
    email, password, name, lastname
  ) => api.post<UserType>(
    '/signup', {
      email, password, name, lastname
    }
  ),

  auth: () => getAuthRefresh(),

  logout: () => api.post('/logout')
};

export default authApi;
