import axios, { AxiosResponse } from 'axios';

import { FieldsType } from 'ducks/form';

type FormApiType = {
  getForm: () => Promise<AxiosResponse<FieldsType>>;
};

const formApi: FormApiType = {
  getForm: (): Promise<AxiosResponse<FieldsType>> => axios.get('http://localhost:3000/data.json')
};

export default formApi;
