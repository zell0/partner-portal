import { FC } from 'react';

import { Container } from 'styles/common';
import Title from 'components/elements/Title';
import Subtitle from 'components/elements/Subtitle';
import Stage from 'components/Stage';
import MainFormContainer from 'components/forms/MainFormContainer';
import Aside from 'components/Aside';

import {
  TopSide,
  WrapperSide,
  LeftSide,
  RightSide
} from './styles';

const MainPage: FC = () => (
  <>
    <Container>
      <Title align="center" bold>Buy New Licenses</Title>
      <Subtitle align="center" paragraph marginbottom="large">
        Request a custom quote to discover the price of NAKIVO Backup & Replication for your particular environment
      </Subtitle>
    </Container>
    <TopSide>
      <Container>
        <Stage />
      </Container>
    </TopSide>
    <Container>
      <WrapperSide>
        <LeftSide>
          <MainFormContainer />
        </LeftSide>
        <RightSide>
          <Aside />
        </RightSide>
      </WrapperSide>
    </Container>
  </>
);

export default MainPage;
