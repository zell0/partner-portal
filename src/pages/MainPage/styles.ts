import styled from 'styled-components/macro';

import bgImage from 'assets/img/bg/stage.jpg';

export const TopSide = styled.div`
  padding: 22px ${({ theme }) => theme.common.indent}px;;
  background: url(${bgImage}) no-repeat center;
  background-size: cover;
`;

export const WrapperSide = styled.div`
  display: grid;
  grid-template-columns: 1fr 300px;
  grid-gap: ${({ theme }) => theme.common.gutter}px;
  margin: 80px 0;
`;

export const LeftSide = styled.main``;

export const RightSide = styled.aside``;
