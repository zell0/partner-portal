import { FC } from 'react';

import Title from 'components/elements/Title';

const NotFoundPage: FC = () => (
  <Title align="center">Page not found</Title>
);

export default NotFoundPage;
