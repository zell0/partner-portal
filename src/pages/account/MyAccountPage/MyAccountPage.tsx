import { FC } from 'react';

import { Container } from 'styles/common';
import Title from 'components/elements/Title';
import List from 'components/List';
import UserInfoContainer from 'components/UserInfoContainer';
import userAccountList from 'utils/data/userAccountList';

import {
  WrapperSide, LeftSide, RightSide
} from './styles';

const MyAccountPage: FC = () => (
  <Container>
    <Title align="center" bold>My account</Title>
    <WrapperSide>
      <LeftSide>
        <List list={userAccountList} />
      </LeftSide>
      <RightSide>
        <UserInfoContainer />
      </RightSide>
    </WrapperSide>
  </Container>
);

export default MyAccountPage;
