import styled from 'styled-components/macro';

export const WrapperSide = styled.div`
  display: grid;
  grid-template-columns: 1fr 360px;
  grid-gap: ${({ theme }) => theme.common.gutter}px;
  margin: 80px 0;
`;

export const LeftSide = styled.main``;

export const RightSide = styled.aside``;
