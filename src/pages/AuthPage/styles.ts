import styled from 'styled-components/macro';

import { device } from 'constants/device';

export const Row = styled.div`
  display: grid;
  grid-template-columns: auto minmax(290px, 350px);
  grid-gap: ${({ theme }) => theme.common.gutter}px;

  ${device.tablet} {
    grid-template-columns: 1fr;
    justify-items: center;
    padding-bottom: ${({ theme }) => theme.common.gutter}px;
  }
`;

export const Left = styled.div`

  ${device.tablet} {
    text-align: center;
  }
`;

export const Right = styled.div`

  ${device.tablet} {
    width: 100%;
    max-width: 350px;
  }
`;
