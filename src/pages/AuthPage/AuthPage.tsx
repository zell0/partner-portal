import { FC } from 'react';
import { useLocation } from 'react-router-dom';

import Title from 'components/elements/Title';
import Subtitle from 'components/elements/Subtitle';
import LoginFormContainer from 'components/forms/auth/LoginFormContainer';
import RegisterFormContainer from 'components/forms/auth/RegisterFormContainer';
import theme from 'constants/theme';

import { Container } from 'styles/common';
import { Row, Left, Right } from './styles';

const AuthPage: FC = () => {
  const { pathname } = useLocation();

  return (
    <Container>
      <Row>
        <Left>
          <Title color={theme.colors.light}>Welcome to the NAKIVO Partner Portal</Title>
          <Subtitle marginbottom="small">NAKIVO is a privately held company that delivers an all-in-one data protection and disaster recovery software solution for SMBs and large enterprises.</Subtitle>
        </Left>
        <Right>
          {
            pathname === '/login/'
              ? <LoginFormContainer />
              : <RegisterFormContainer />
          }
        </Right>
      </Row>
    </Container>
  );
};

export default AuthPage;
