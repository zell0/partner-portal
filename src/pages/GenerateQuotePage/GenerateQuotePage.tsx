import { FC } from 'react';

import { Container } from 'styles/common';
import Title from 'components/elements/Title';

const GenerateQuotePage: FC = () => (
  <Container>
    <Title align="center">Generate Quote</Title>
  </Container>
);

export default GenerateQuotePage;
