import { combineReducers } from 'redux';

import user from 'ducks/user';
import form from 'ducks/form';

const rootReducer = combineReducers({
  user,
  form
});

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
