import { all } from 'redux-saga/effects';

import { watchUser } from 'ducks/user';
import { watchForm } from 'ducks/form';

export default function* rootSaga(): Generator {
  yield all([
    watchUser(),
    watchForm()
  ]);
}
