import { useMemo } from 'react';
import { useDispatch, useSelector, TypedUseSelectorHook } from 'react-redux';
import { bindActionCreators, ActionCreatorsMapObject } from 'redux';
import { put, call, takeLatest } from 'redux-saga/effects';
import { SagaIterator } from '@redux-saga/core';
import produce, { Immutable, Draft } from 'immer';

import { AppState } from 'store/reducer';
import formApi from 'services/form';

/**
 * Types
 */
export type FieldsType = FieldType[];
type FieldType = {
  id: number;
  name: string;
  value: number;
  type: string;
  description?: string;
  category?: string;
};

/**
 * Action types
 */
const FORM_FETCH_REQUEST = 'FORM_FETCH_REQUEST';
type FormFetchRequestAction = {
  type: typeof FORM_FETCH_REQUEST;
};

const FORM_FETCH_SUCCESS = 'FORM_FETCH_SUCCESS';
type FormFetchSuccessAction = {
  type: typeof FORM_FETCH_SUCCESS;
};

const FORM_FETCH_FAILURE = 'FORM_FETCH_FAILURE';
type FormFetchFailureAction = {
  type: typeof FORM_FETCH_FAILURE;
  error: string;
};

const FORM_SET = 'FORM_SET';
type FormSetAction = {
  type: typeof FORM_SET;
  payload: FieldsType;
};

type FormActionTypes =
  | FormFetchRequestAction
  | FormFetchSuccessAction
  | FormFetchFailureAction
  | FormSetAction;

/**
 * Actions
 */
export const formFetchRequest = (): FormActionTypes => ({
  type: FORM_FETCH_REQUEST
});

export const formFetchSuccess = (): FormActionTypes => ({
  type: FORM_FETCH_SUCCESS
});

export const formFetchFailure = (error: string): FormActionTypes => ({
  type: FORM_FETCH_FAILURE,
  error
});

export const formSet = (payload: FieldsType): FormActionTypes => ({
  type: FORM_SET,
  payload
});

/**
 * Reducer
 */
type FormState = Immutable<{
  fields: FieldsType;
  isLoading: boolean;
  error: false | string;
}>;

const initialState: FormState = {
  fields: [],
  isLoading: false,
  error: false
};

const formReducer = produce((state: Draft<FormState>, action: FormActionTypes) => {
  switch (action.type) {
    case FORM_FETCH_REQUEST:
      state.isLoading = true;
      state.error = false;
      state.fields = [];
      break;

    case FORM_FETCH_SUCCESS:
      state.isLoading = false;
      state.error = false;
      break;

    case FORM_FETCH_FAILURE:
      state.isLoading = false;
      state.error = action.error;
      break;

    case FORM_SET:
      state.fields = action.payload;
      break;

    default:
      return state;
  }

  return state;
}, initialState);

export default formReducer;

/**
 * Hooks
 */
const useTypedSelector: TypedUseSelectorHook<AppState> = useSelector;

export const useForm = (): FormState => {
  const { fields, isLoading, error } = useTypedSelector((state) => state.form);

  return { fields, isLoading, error };
};

export const useFormActions = (): ActionCreatorsMapObject => {
  const dispatch = useDispatch();

  return useMemo(() => bindActionCreators(
    {
      formFetchRequest,
      formFetchSuccess,
      formFetchFailure,
      formSet
    },
    dispatch
  ), [dispatch]);
};

/**
 * Sagas
 */
function* fetchFormWorker(): SagaIterator {
  try {
    const form = yield call(formApi.getForm);
    yield put(formSet(form.data));
    yield put(formFetchSuccess());
  } catch (e) {
    if (e instanceof Error) yield put(formFetchFailure(e.message));
  }
}

export function* watchForm(): SagaIterator {
  yield takeLatest(FORM_FETCH_REQUEST, fetchFormWorker);
}
