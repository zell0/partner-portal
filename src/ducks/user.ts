import { useMemo } from 'react';
import { useDispatch, useSelector, TypedUseSelectorHook } from 'react-redux';
import { bindActionCreators, ActionCreatorsMapObject } from 'redux';
import { put, call, takeLatest } from 'redux-saga/effects';
import { SagaIterator } from '@redux-saga/core';
import produce, { Immutable, Draft } from 'immer';
import request, { AxiosResponse } from 'axios';

import { AppState } from 'store/reducer';
import authApi from 'services/auth';

/**
 * Types
 */
export type UserType = {
  accessToken: string;
  refreshToken: string;
  user: {
    id: string;
    role: string;
    name: string;
    lastname: string;
    email: string;
    isActivated: string;
    phone?: string;
    address?: string;
    avatar?: string;
  }
};

export type UserLoginType = {
  email: string;
  password: string;
};

export type UserRegType = {
  name: string;
  lastname: string;
  email: string;
  password: string;
};

/**
 * Action types
 */
const USER_LOGIN_REQUEST = 'USER_LOGIN_REQUEST';
type UserLoginRequestAction = {
  type: typeof USER_LOGIN_REQUEST;
  payload: UserLoginType;
};

const USER_REG_REQUEST = 'USER_REG_REQUEST';
type UserRegRequestAction = {
  type: typeof USER_REG_REQUEST;
  payload: UserRegType;
};

const USER_REQUEST_FAILURE = 'USER_REQUEST_FAILURE';
type UserRequestFailureAction = {
  type: typeof USER_REQUEST_FAILURE;
  error: string;
};

const USER_SET = 'USER_SET';
type UserSetAction = {
  type: typeof USER_SET;
  payload: UserType;
};

const USER_AUTH = 'USER_AUTH';
type UserAuthAction = {
  type: typeof USER_AUTH;
};

const USER_LOGOUT = 'USER_LOGOUT';
type UserLogoutAction = {
  type: typeof USER_LOGOUT;
};

type UserActionTypes =
  | UserLoginRequestAction
  | UserRegRequestAction
  | UserRequestFailureAction
  | UserSetAction
  | UserAuthAction
  | UserLogoutAction;

/**
 * Actions
 */
export const userLoginRequest = (payload: UserLoginType): UserActionTypes => ({
  type: USER_LOGIN_REQUEST,
  payload
});

export const userRegRequest = (payload: UserRegType): UserActionTypes => ({
  type: USER_REG_REQUEST,
  payload
});

export const userRequestFailure = (error: string): UserActionTypes => ({
  type: USER_REQUEST_FAILURE,
  error
});

export const userSet = (payload: UserType): UserActionTypes => ({
  type: USER_SET,
  payload
});

export const userAuth = (): UserActionTypes => ({
  type: USER_AUTH
});

export const userLogout = (): UserActionTypes => ({
  type: USER_LOGOUT
});

/**
 * Reducer
 */
type UserState = Immutable<{
  user: null | UserType;
  isAuth: boolean;
  isLoading: boolean;
  error: false | string;
}>;

const initialState: UserState = {
  user: null,
  isAuth: !!localStorage.getItem('token'),
  isLoading: false,
  error: false
};

const userReducer = produce((state: Draft<UserState>, action: UserActionTypes) => {
  switch (action.type) {
    case USER_LOGIN_REQUEST:
    case USER_REG_REQUEST:
      state.isLoading = true;
      state.error = false;
      state.user = null;
      break;

    case USER_REQUEST_FAILURE:
      state.user = null;
      state.isLoading = false;
      state.isAuth = false;
      state.error = action.error;
      break;

    case USER_AUTH:
      state.isLoading = true;
      state.error = false;
      break;

    case USER_SET:
      state.user = action.payload;
      state.isAuth = true;
      state.isLoading = false;
      state.error = false;
      break;

    case USER_LOGOUT:
      state.user = null;
      state.isAuth = false;
      break;

    default:
      return state;
  }

  return state;
}, initialState);

export default userReducer;

/**
 * Hooks
 */
const useTypedSelector: TypedUseSelectorHook<AppState> = useSelector;

export const useUser = (): UserState => {
  const {
    user, isAuth, isLoading, error
  } = useTypedSelector((state) => state.user);

  return {
    user, isAuth, isLoading, error
  };
};

export const useUserActions = (): ActionCreatorsMapObject => {
  const dispatch = useDispatch();

  return useMemo(() => bindActionCreators(
    {
      userLoginRequest,
      userRegRequest,
      userRequestFailure,
      userSet,
      userAuth,
      userLogout
    },
    dispatch
  ), [dispatch]);
};

/**
 * Sagas
 */
function* handleFetchUser(apiFunc: (...args: string[]) => Promise<AxiosResponse<UserType>>, ...data: string[]): unknown {
  try {
    const user = yield call(apiFunc, ...data);
    yield call([localStorage, 'setItem'], 'token', user.data.accessToken);
    yield put(userSet(user.data));
  } catch (e) {
    if (request.isAxiosError(e) && e.response) {
      yield put(userRequestFailure(e.response?.data?.message));
    }
  }
}

function* loginUserWorker({ payload: { email, password } }: UserLoginRequestAction): SagaIterator {
  yield call(handleFetchUser, authApi.login, email, password);
}

function* regUserWorker({
  payload: {
    name, lastname, email, password
  }
}: UserRegRequestAction): SagaIterator {
  yield call(handleFetchUser, authApi.registration, email, password, name, lastname);
}

function* authUserWorker(): SagaIterator {
  try {
    const user = yield call(authApi.auth);
    yield call([localStorage, 'setItem'], 'token', user.data.accessToken);
    yield put(userSet(user.data));
  } catch (e) {
    if (request.isAxiosError(e) && e.response) yield put(userRequestFailure(e.response?.data?.message));
  }
}

function* logoutUserWorker(): SagaIterator {
  try {
    yield call(authApi.logout);
    yield call([localStorage, 'removeItem'], 'token');
  } catch (e) {
    if (request.isAxiosError(e) && e.response) yield put(userRequestFailure(e.response?.data?.message));
  }
}

export function* watchUser(): SagaIterator {
  yield takeLatest(USER_LOGIN_REQUEST, loginUserWorker);
  yield takeLatest(USER_REG_REQUEST, regUserWorker);
  yield takeLatest(USER_AUTH, authUserWorker);
  yield takeLatest([USER_REQUEST_FAILURE, USER_LOGOUT], logoutUserWorker);
}
