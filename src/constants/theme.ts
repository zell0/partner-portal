const theme = {
  common: {
    containerWidth: 1200,
    gutter: 30,
    indent: 15
  },
  fonts: {
    main: '\'Arial\', sans-serif'
  },
  colors: {
    main: '#000000',
    secondary: '#fa8500',
    accent: '#0d79fa',
    accentSec: '#66b57c',
    border: '#d9e1eb',
    borderLight: '#f2f5fa',
    error: '#fa0800',
    light: '#ffffff',
    bgLight: '#d9eafe',
    title: '#476E9A'
  }
};

export type Theme = typeof theme;
export default theme;
