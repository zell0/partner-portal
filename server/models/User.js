const { Schema, model } = require('mongoose');

const User = new Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  name: { type: String, required: true },
  lastname: { type: String, required: true },
  role: { type: String, default: 'client' },
  avatar: { type: String, default: '' },
  isActivated: { type: Boolean, default: false },
  activationLink: { type: String }
});

module.exports = model('User', User);
