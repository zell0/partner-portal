const Router = require('express').Router;
const { body } = require('express-validator');

const userController = require('../controllers/user');
const formController = require('../controllers/form');
const authMiddleware = require('../middlewares/auth.middleware');

const router = new Router();

router.post(
  '/signup',
  body('email', 'Incorrect email').isEmail(),
  body('password', 'Password must be longer than 5 and shorter than 20').isLength({ min: 6, max: 20 }),
  body('name', 'Name must be longer than 1 and shorter than 20').isLength({ min: 2, max: 20 }),
  body('lastname', 'Lastname must be longer than 1 and shorter than 20').isLength({ min: 2, max: 20 }),
  userController.registration
);

router.post(
  '/login',
  body('email', 'Incorrect email').isEmail(),
  body('password', 'Password must be longer than 5 and shorter than 20').isLength({ min: 6, max: 20 }),
  userController.login
);

router.get('/activate/:link', userController.activate);

router.get('/refresh', userController.refresh);

router.post('/logout', userController.logout);

router.get('/form', authMiddleware, formController.getForm);

module.exports = router;
