class FormService {
  async getForm() {
    return await FormModel.find();
  }
}

module.exports = new FormService();
