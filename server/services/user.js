const bcrypt = require('bcryptjs');
const uuid = require('uuid');

const UserModel = require('../models/User');
const mailService = require('../services/mail');
const tokenService = require('../services/token');
const UserDto = require('../dtos/user');
const ApiError = require('../exceptions/ApiError');

const generateTokens = async (user) => {
  const userDto = new UserDto(user);
  const tokens = tokenService.generateTokens({ ...userDto });
  await tokenService.saveToken(userDto.id, tokens.refreshToken);

  return {
    ...tokens,
    user: userDto
  };
};

class UserService {
  async registration(email, password, name, lastname) {
    const candidate = await UserModel.findOne({ email });

    if (candidate) {
      throw ApiError.BadRequest(`User with email ${email} already exists`);
    }

    const hashPassword = await bcrypt.hash(password, 8);
    const activationLink = uuid.v4();

    const user = await UserModel.create({ email, password: hashPassword, name, lastname, activationLink });
    await mailService.sendActivationMail(email, `${process.env.API_URL}/activate/${activationLink}`);

    return generateTokens(user);
  }

  async login(email, password) {
    const user = await UserModel.findOne({ email });
    if (!user) {
      throw ApiError.BadRequest(`User ${email} not found`);
    }

    const isPasswordValid = bcrypt.compareSync(password, user.password);
    if (!isPasswordValid) {
      throw ApiError.BadRequest('Invalid password');
    }

    return generateTokens(user);
  }

  async activate(activationLink) {
    const user = await UserModel.findOne({ activationLink });

    if (!user) {
      throw ApiError.BadRequest('Incorrect link');
    }

    user.isActivated = true;

    await user.save();
  }

  async refresh(refreshToken) {
    if (!refreshToken) {
      throw ApiError.Unauthorized();
    }

    const user = tokenService.validateRefreshToken(refreshToken);
    const tokenFromDb = tokenService.findToken(refreshToken);
    if (!user || !tokenFromDb) {
      throw ApiError.Unauthorized();
    }

    const userFromDb = await UserModel.findById(user.id);

    return generateTokens(userFromDb);
  }

  async logout(refreshToken) {
    return await tokenService.removeToken(refreshToken);
  }

  async getForm() {
    return await UserModel.find();
  }
}

module.exports = new UserService();
