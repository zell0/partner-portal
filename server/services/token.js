const jwt = require('jsonwebtoken');

const tokenModel = require('../models/Token');

class TokenService {
  generateTokens(payload) {
    const accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN, { expiresIn: '1h' });
    const refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN, { expiresIn: '30d' });

    return {
      accessToken,
      refreshToken
    };
  }

  validateAccessToken(token) {
    return jwt.verify(token, process.env.ACCESS_TOKEN);
  }

  validateRefreshToken(token) {
    return jwt.verify(token, process.env.REFRESH_TOKEN);
  }

  async saveToken(userId, refreshToken) {
    const tokenData = await tokenModel.findOne({ user: userId })

    if (tokenData) {
      tokenData.refreshToken = refreshToken;

      return tokenData.save();
    }

    return await tokenModel.create({ user: userId, refreshToken });
  }

  async findToken(refreshToken) {
    return await tokenModel.findOne({ refreshToken });
  }

  async removeToken(refreshToken) {
    return await tokenModel.deleteOne({ refreshToken });
  }
}

module.exports = new TokenService();
