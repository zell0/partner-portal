const formService = require('../services/user');

class FormController {
  async getForm(req, res, next) {
    try {
      const form = await formService.getForm();

      return res.json(form);
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new FormController();
