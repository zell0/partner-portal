const ApiError = require('../exceptions/ApiError');
const tokenService = require('../services/token');

function auth(req, res, next) {
  if (req.method === 'OPTIONS') {
    return next();
  }

  try {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return next(ApiError.Unauthorized());
    }

    const accessToken = authHeader.split(' ')[1];
    if (!accessToken) {
      return next(ApiError.Unauthorized());
    }

    const userData = tokenService.validateAccessToken(accessToken);
    if (!userData) {
      return next(ApiError.Unauthorized());
    }
    
    console.log(userData);

    req.user = userData;
    next();
  } catch (e) {
    return next(ApiError.Unauthorized());
  }
}

module.exports = auth;
