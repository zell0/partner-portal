module.exports = class UserDto {
  id;
  email;
  name;
  lastname;
  isActivated;

  constructor(model) {
    this.id = model._id;
    this.email = model.email;
    this.name = model.name;
    this.lastname = model.lastname;
    this.isActivated = model.isActivated;
  }
}
